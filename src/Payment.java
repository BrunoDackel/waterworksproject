public class Payment {

    private static void getRelevantInformationFromCSV() {
        //TODO not Implemented
        //No idea how this should look like... we should just simulate this method
        //But in theory this would write to the Transaction table
    }

    /**
     * Here we will handle personal payments. We validate the input and then insert into the Database.
     *
     * @param paymentID  the payment number, must be valid
     * @param bank       the bank name, must be less than 31 chars
     * @param amount     the amount transferred, must be non negative
     * @param type       the transaction type (Personal, Giro, Bank), must be less than 31 chars
     * @param customerID the CPR-NR, must be valid
     */
    //By Sven Büchner
    private static void insertTransaction(String paymentID, String bank, int amount, String type, String customerID, String date) {
        //validate
        if (ValidateHelper.validatePaymentID(paymentID) || ValidateHelper.validateCPR(customerID) || ValidateHelper.validateDate(date)) {
            return;
        }
        if (!(amount >= 0)) {
            UserInterface.alertWindow("Error", "Payment can't be less then zero!");
            return;
        }
        if (bank.length() > 30) {
            UserInterface.alertWindow("Error", "Bank name to long!");
            return;
        }
        if (type.length() > 30) {
            UserInterface.alertWindow("Error", "Transaction type name to long!");
            return;
        }
        //Add this transation to the tblTransaktion table and make relation tblPayment
        String[] insertValues = {bank, Integer.toString(amount), type, customerID, paymentID, date};
        DatabaseHelper.insertToTable("tblTransaction", insertValues);
        //TODO not tested
    }

    private static void chekForExtraFee(String date) {
        //TODO not Implemented(This is a hard one i will look at that later)
        //reading card will be send out at yyyy-01-01 and yyyy-07-01 (make incomplete payments)
        //A month later staff will be send and fee will be added
        //A month later first reminder, 2 month later second reminder, 3m onth later last reminder 4 month later close water
        String monthAndDay = date.substring(4,7);
        if(monthAndDay.equals("01-01") || monthAndDay.equals("07-01")){

        }

        //Go through all payments

        //Chek if the reading card is to late
        //If it is send staff (reminder class)
        //remember to add a fee

        //Chek if the payment is late
        //if it is sendDueReminder
    }

    private static void sendDueReminder(int PaymentID) {
        //add a fee to the payment
        //Find and calculate how much is due
        //Gather all information to send the reminder
        //invoke reminder method
        //TODO not Implemented(Look at that in cinunctiion with chekForExtrFee)
    }

    /**
     * In this method we will create and insert a new Payment to tblPayment. If the ReadingCard is valid we know that we can reach the Drainage tax/water Tax.
     * This is because neither tblWaterMeter, tbl Building or tblDrainage/Water/tax allow null values in the relevant fields. The Payment will be created with zero fee, use the addFee method to create a fee.
     *
     * @param readingID The reading card returned from the Customer
     * @param waterID   The Freshwater ID the customer get
     */
    //Sven Büchner
    public static void createPayment(String readingID, String waterID) {
        //Validate the readingID
        if (ValidateHelper.validateRadingCardID(readingID) || ValidateHelper.validateWaterID(waterID)) {
            return;
        }
        //Finding the drainageTax
        String drainageTax = DatabaseHelper.findDrainageTaxByWaterReadingID(readingID);
        //Finding the waterTax
        String waterTax = DatabaseHelper.findWaterTaxByWaterReadingID(readingID);
        //Insertion
        String[] insertValues = {drainageTax, waterTax, readingID, waterID, "0"};
        DatabaseHelper.insertToTable("tblPayment", insertValues);
        //TODO not tested
    } //TODO Chek flow

    /**
     * This will create an incomplete payment. An incomplete payment will only have what water the payment is for and a fee. fldDrainageTax, fldWaterTax and fldReading will be initialized to NULL
     * and need to be added as soon as the reading card will be added. This will be handeled of ConsumptionData.addReadingCardToPayment()
     *
     * @param fee     the fee the Payment will be initialized to.
     * @param waterID The Freshwater ID the customer get
     */
    /*There is actually are Huge problem with that here. We cant trace a Payment back to a customer, house or any way to the rest of the Databse.
    The solution here is that we assume that the system will get output when the Staff collected the reading.
    */
    //By Sven Büchner
    private static void createIncompletePayment(int fee, String waterID) { //TODO Chek flow
        //Validate the WaterID
        if (ValidateHelper.validateWaterID(waterID)) {
            return;
        }
        String[] insertValues = {null, null, null, waterID, Integer.toString(fee)};
        DatabaseHelper.insertToTable("tblTable", insertValues);
        //TODO not tested
    }

    /**
     * Wee add a fee to an existing  payment
     *
     * @param paymentID the payment id, must exist in the DB
     * @param fee       the fee, must be equal or greater than zero
     */
    //By Sven Büchner
    public static void addFee(String paymentID, int fee) {
        //Validate paymentID and fee
        if (ValidateHelper.validatePaymentID(paymentID)) {
            return;
        }
        if (!(fee >= 0)) {
            UserInterface.alertWindow("Error", "Fee can't be less then zero!");
            return;
        }
        //get existing fee
        int oldFee;
        String[] tempArray = DatabaseHelper.selectRow("tblPayment", "fldID", paymentID);
        oldFee = Integer.parseInt(tempArray[5]);
        //add the fee ontop
        int newFee;
        newFee = oldFee + fee;
        //Update the payment
        DatabaseHelper.updateRow("tblPayment", "fldID", paymentID, "fldFee", Integer.toString(newFee));
        //TODO not tested
    }
}

