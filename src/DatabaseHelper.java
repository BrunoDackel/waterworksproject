import java.util.ArrayList;
//TODO Thoose method should realy chek if a string is empty
public class DatabaseHelper {

    /**
     * We will insert values into a specified table. Note that this method will not validate any input.
     * @param tblTable the table we want to insert to
     * @param values A string array with the values we want to insert
     */
    //By Sven Büchner
    public static void insertToTable(String tblTable, String[] values) {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT into ");
        builder.append(tblTable);
        builder.append(" VALUES(");
        for (String str: values) {
            builder.append("'");
            builder.append(str);
            builder.append("',");
        }
        builder.delete(builder.length()-1,builder.length()); //Delete last the last ,
        builder.append(")");

        DB.insertSQL(builder.toString());
    }

    /**
     * We update a single row with a single value
     *
     * @param tblTable           the table where update takes place
     * @param fldPrimaryKeyField the field where the Primary key is placed
     * @param primaryKey         the unique key we will identify the row with
     * @param targetField        the field we want to update the value of
     * @param newValue           the value we will insert into the table
     */
    //By Sven Büchner
    public static void updateRow(String tblTable, String fldPrimaryKeyField, String primaryKey, String targetField, String newValue) {
        //Todo we can return bool to the user
        DB.updateSQL("UPDATE " + tblTable + " SET " + targetField + " = '" + newValue + "' WHERE " + fldPrimaryKeyField + " = '" + primaryKey + "'");
    }

    private static ArrayList selectFromTable(String table, String statment) {
        ArrayList tempArray = new ArrayList();
        return tempArray;
        //TODO not Implemented(We might not use it, very low priority)
    }

    /**
     * We will return a single row from a specified table.
     *
     * @param tblTable           the table we want to access
     * @param fldPrimaryKeyField the field that houses the primary key
     * @param primaryKey         the key we want identify the to be returned row with
     * @return Array of the same length as columns filled with Strings
     */
    //By Sven Büchner
    public static String[] selectRow(String tblTable, String fldPrimaryKeyField, String primaryKey) {
        String[] returnArray = new String[DatabaseHelper.numberOfColumns(tblTable)]; //Init a array with space for all columns
        DB.selectSQL("SELECT * FROM " + tblTable + " WHERE " + fldPrimaryKeyField + " = '" + primaryKey + "'");
        int count = 0;
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else {
                returnArray[count] = data;
                count++;
            }
        } while (true);
        return returnArray;
    }

    /**
     * Searches a single column of a table for the specified keyword. Note that the table and field need to have tbl and fld prefix
     *
     * @param tblTable the table to be searched
     * @param fldField the column to be searched
     * @param keyWord  what we search for
     * @return true if the keyword was found
     */
    //By Sven Büchner
    public static boolean searchTableForExistance(String tblTable, String fldField, String keyWord) {
        boolean returnBoolean = false;
        DB.selectSQL("SELECT COUNT(" + fldField + ") FROM " + tblTable + " WHERE " + fldField + "='" + keyWord + "'");
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) { //also should that not be 0 cause string
                break;
            } else if (Integer.parseInt(data) > 0) {
                returnBoolean = true;
            }
        } while (true);
        return returnBoolean;
    }

    /**
     * Searches a single column of a table for the specified keyword. Note that the table and field need to have tbl and fld prefix
     *
     * @param tblTable the table to be searched
     * @param fldField the column to be searched
     * @param keyNumber  what we search for
     * @return true if the keyword was found
     */
    //By Sven Büchner
    public static boolean searchTableForExistance(String tblTable, String fldField, int keyNumber) {
        boolean returnBoolean = false;
        DB.selectSQL("SELECT COUNT(" + fldField + ") FROM " + tblTable + " WHERE " + fldField + "=" + keyNumber + "");
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else if (Integer.parseInt(data) > 0) { // i saw i debug that i get a 5/.... how can i get fix the / away
                returnBoolean = true;
            }
        } while (true);
        return returnBoolean;
    }

    public static int countInTable(String tblTable, String fldFIeld, String keyWord){
        //TODO Not implemented might not need that
        return 0;
    }

    /**
     * get how many columns (fields) there are in a specified table
     *
     * @param tblTable the table we want to amount of fld´s from
     * @return the number of columns
     */
    //By Sven Büchner
    private static int numberOfColumns(String tblTable) {
        int returnInt;
        DB.selectSQL("SELECT * FROM " + tblTable); //We need to select the table before we can get the columns
        returnInt = DB.getNumberOfColumns();
        return returnInt;
    }

    /**
     * Find a drainage tax attached to a house by entering the waterMeterID
     * @param waterMeterID the waterMeter attached to a building
     * @return the Drainage tax
     */
    //By Sven Büchner
    public static String findDrainageTaxByWaterReadingID(String waterMeterID){
        String drainageTax ="";
        DB.selectSQL("SELECT fldDrainageTax FROM tblBuilding WHERE fldID = (SELECT fldID FROM tblWaterMeter WHERE fldID = (SELECT fldWaterMeter FROM tblReading WHERE fldID ='"+waterMeterID+"'))");
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else {
                drainageTax = data;

            }
        } while (true);
        return drainageTax;
    }

    /**
     * Find a water tax attached to a house by entering the waterMeterID
     * @param waterMeterID the waterMeter attached to a building
     * @return the Drainage tax
     */
    //By Sven Büchner
    public static String findWaterTaxByWaterReadingID(String waterMeterID){
        String waterTax ="";
        DB.selectSQL("SELECT fldDrainageTax FROM tblBuilding WHERE fldID = (SELECT fldID FROM tblWaterMeter WHERE fldID = (SELECT fldWaterMeter FROM tblReading WHERE fldID ='"+waterMeterID+"'))");
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else {
                waterTax = data;

            }
        } while (true);
        return waterTax;
    }

    public static double findWaterTaxRateByPaymentID(String paymentID){
        double rate = 0;
        DB.selectSQL("SELECT fldTaxRate FROM tblDrainageTax WHERE fldID = (SELECT fldReading FROM tblPayment WHERE fldID ='"+paymentID+"')");
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else {
                rate = Double.parseDouble(data);

            }
        } while (true);
        return rate;
    }

    public static double findDrainageTaxRateByPaymentID(String paymentID){
        double rate = 0;
        DB.selectSQL("SELECT fldTaxRate FROM tblWaterTax WHERE fldID = (SELECT fldReading FROM tblPayment WHERE fldID ='"+paymentID+"')");
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else {
                rate = Double.parseDouble(data);

            }
        } while (true);
        return rate;
    }

    public static double findAmountWaterUsedByPaymentID(String paymentID){
        double amountUsed = 0;
        DB.selectSQL("SELECT fldAmountWater FROM tblReading WHERE fldID = (SELECT fldReading FROM tblPayment WHERE fldID ='"+paymentID+"')");
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else {
                amountUsed = Double.parseDouble(data);

            }
        } while (true);
        return amountUsed;
    }

    public static double findWaterCostByPaymentID(String paymentID){
        double cost = 0;
        DB.selectSQL("SELECT fldCost FROM tblWater WHERE fldID = (SELECT fldWater FROM tblPayment WHERE fldID ='"+paymentID+"')");
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else {
                cost = Double.parseDouble(data);

            }
        } while (true);
        return cost;
    }

    public static double amountDueByPaymentID(String paymentID){
        double waterUsed = DatabaseHelper.findAmountWaterUsedByPaymentID(paymentID);
        double waterCost = DatabaseHelper.findWaterCostByPaymentID(paymentID);
        double drainageTax = DatabaseHelper.findDrainageTaxRateByPaymentID(paymentID);
        double waterTax = DatabaseHelper.findWaterTaxRateByPaymentID(paymentID);
        double calculation = (waterUsed * waterCost) + (waterCost*drainageTax) + (waterCost*waterTax);
        return calculation;
    }
}
