import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.layout.GridPane;

public class UserInterface extends Application {

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("JavaFX Water Work");

        primaryStage.show();

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));


        Button btn = new Button("Sign in");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btn);
        grid.add(hbBtn, 1, 4);


        Scene scene = new Scene(grid, 300, 275);
        primaryStage.setScene(scene);

        Text scenetitle = new Text("System");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label addOwner = new Label("Add Customer to building:");
        grid.add(addOwner, 0, 1);

        TextField addOwnerCPRNR = new TextField();
        grid.add(addOwnerCPRNR, 1, 1);

        TextField addOwnerBuildingID = new TextField();
        grid.add(addOwnerBuildingID, 2, 1);

        Button addOwerButton = new Button("add");
        HBox hbaddOwerButton = new HBox(10);
        hbaddOwerButton.setAlignment(Pos.BOTTOM_RIGHT);
        hbaddOwerButton.getChildren().add(addOwerButton);
        grid.add(hbaddOwerButton, 3, 1);

        Label removeOwner = new Label("Remove Customer from building:");
        grid.add(removeOwner, 0, 2);

        PasswordField pwBox = new PasswordField();
        grid.add(pwBox, 1, 2);


    }

    public static void alertWindow(String title, String content){
        //Todo Not implemnted(only for swag points)
        System.out.println(content);
    }

    public static boolean decisionWindow(String title, String content){
        //Todo not implemented(Only for swag points)
        return true;
    }
}
