public class Administrate {
    /**
     * Changing the WaterTax for an existing building ID.
     * If building id and waterTax does not exist it will prompt a error message
     *
     * @param BuildingID The id of the building we want to update
     * @param newType    The new water tax.
     */
    //By Anders, Karsten, Wisam
    private static void changeBuildingsTaxType(int BuildingID, int newType) {
        //does the Building ID exist?
        if (!DatabaseHelper.searchTableForExistance("tblBuilding", "fldID", BuildingID)) {
            UserInterface.alertWindow("Error", "Building id not found");
            return;
        }
        //Does the Type Exist?
        if (!DatabaseHelper.searchTableForExistance("tblTaxationType", "fldID", newType)) {
            UserInterface.alertWindow("Error", "Water tax does not exist");
            return;
        }

        //Update the segment type
        DatabaseHelper.updateRow("tblBuilding", "fldID", Integer.toString(BuildingID), "fldWaterTax", Integer.toString(newType));
        //TODO not Testet
    }

    /**
     * Changing the drainageTax for an existing building ID.
     * If building id and waterTax does not exist it will prompt a error message
     *
     * @param BuildingID The id of the building we want to update
     * @param newType    The new drainage type
     */
    //By Anders, Karsten, Wisam
    private static void changeBuildingsDrainageType(int BuildingID, String newType) {
        if (!DatabaseHelper.searchTableForExistance("tblBuilding", "fldID", BuildingID)) {
            UserInterface.alertWindow("Error", "Building id not found");
            return;
        }
        //Does the Type Exist?
        if (!DatabaseHelper.searchTableForExistance("tblDrainageTax", "fldID", newType)) {
            UserInterface.alertWindow("Error", "TaxRate not found");
            return;
        }

        //Update the segment type
        DatabaseHelper.updateRow("tblBuilding", "fldID", Integer.toString(BuildingID), "fldDrainageTax", (newType));
        //TODO not Testet
    }

    //Hard
    private static void changeTaxTypeForAll(String oldType, String newType) {
        //Does the old and new type exist?
        //find all Buildings with the oldType
        //Update all Building with oldType to newType
        //Todo not implemnted(low priority)
    }

    //Hard
    private static void changeDrainageTypeForAll(String oldType, String newType) {
        //Does the old and new type exist?
        //find all Buildings with the oldType
        //Update all Building with oldType to newType
        //Todo not implemnted(low priority)
    }

    private static void newTax(String name, double taxRate, String Date) {
        //Do we have some naming rules for the name? If yes, validate them
        //Validate that the tax rate is not negative
        //Validate that the Date string is in the correct format
        //Insert into Database
        //TODO not Implemented(low priority)
    }

    private static void newWater(String name, double cost, String Date) {
        //Doe we have some naming rules for the name? If yes, validate them
        //Validate that the cost is not negative
        //Validate that the Date string is in the correct format
        //Insert into Database
        //TODO not Implemented(low)
    }

    /**
     * @param newAdress      New value for fldAddress
     * @param newZip         New value for fldZip
     * @param newCustomerID  New value for fldCustomerID
     * @param newDrainageTax New value for fldDrainageTax
     * @param newWaterTax    New value for fldWaterTax
     */
    //By Anders, Karsten, Wisam
    private static void newBuilding(String newAdress, String newZip, int newCustomerID, int newDrainageTax, int newWaterTax) {
        //Doe we have some naming rules for the name? If yes, validate them

        String strCustomerID = Integer.toString(newCustomerID);
        String strWaterTax = Integer.toString(newWaterTax);


        if (newAdress.length() > 30) {
            UserInterface.alertWindow("Error", "Address too long");
            return;
        }
        if (!ValidateHelper.validateNewZip(newZip))
            UserInterface.alertWindow("Error", "Zip not valid");

        if (!ValidateHelper.validateNewCPR(strCustomerID))
            UserInterface.alertWindow("Error", "Cpr is not valid");

        if (!DatabaseHelper.searchTableForExistance("tblDrainageTax", "fldID", newDrainageTax)) {
            UserInterface.alertWindow("Error", "Input is not a valid Drainage type");
        }
        if (!DatabaseHelper.searchTableForExistance("tblWaterTax", "fldID", strWaterTax)) {
            UserInterface.alertWindow("Error", "Input is not valid");
        }
        String[] inputcollums = {newAdress, newZip, strCustomerID, Integer.toString(newDrainageTax), strWaterTax};

        DatabaseHelper.insertToTable("tblBuilding", inputcollums);
        //TODO not Tested
    }

    //TODO Missing javaDOC!!!!
    //By Wisam
    public static void newZip(String zip, String city) {
        //Validate that the zip is not used already... for whatever reason//Insert into Database
        // TODO not tested
        if (!ValidateHelper.validateNewZip(zip)) {
            UserInterface.alertWindow("Error", "Zip code is not valid");
            return;
        }
        if (city.length() > 30) {
            UserInterface.alertWindow("Error", "City name to long");
            return;
        }

        String[] inputrow = {zip, city};
        DatabaseHelper.insertToTable("tblZip", inputrow);
    }
}