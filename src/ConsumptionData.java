public class ConsumptionData {

    /**
     * This method will create a reading card in the Databse and link it to a payment.
     *
     * @param waterMeterId the waterMeter, may not contain numbers
     * @param date         the date, must be valid format yyyy-mm-dd
     * @param amountWater  the amount water, must be non negative
     * @param waterID      the waterId of the water price, must exist in the database
     */
    //By Sven Büchner
    private static void insertReadingCardData(String waterMeterId, String date, double amountWater, String waterID) { //TODO Chek flow
        //Validate
        if(ValidateHelper.validateWaterMeterID(waterMeterId) || ValidateHelper.validateDate(date) || ValidateHelper.validateWaterID(waterID)){
            return;
        }
        if (amountWater < 0) {
            UserInterface.alertWindow("Error", "Water amount can not be negative");
            return;
        }
        if(!validateConsumptionReadings(waterMeterId,amountWater)){
            return;
        }
        //Insert reading into Database
        String[] insertArray = {waterMeterId, date, Double.toString(amountWater)};
        DatabaseHelper.insertToTable("tblReading", insertArray);
        //Get the reading id. Note that this code is bad and if we ever would multithreaded shit would go south. This only works because we assume there will be no two readings from the same water meter the same day
        DB.selectSQL("SELECT fldID FROM tblReading WHERE fldWaterMeter = '" + waterMeterId + "' AND fldDate = '" + date + "'");
        String readingID = "";
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else {
                readingID = data;
            }
        } while (true);
        //Crate the payment
        Payment.createPayment(readingID, waterID);
        //TODO not tested
    }

    /**
     * We assume that this method will be called after the staff was out and collected the reading card.
     * This method will update an existing payment, das was created from Payment.createIncompletePayment() to be complete
     *
     * @param waterMeterID the water Meter, must exist in Database
     * @param date         the date
     * @param amountWater  how much water awas used
     * @param paymentID    the paymentID
     */
    //By Sven Büchner
    private static void addReadingCardToPayment(String waterMeterID, String date, double amountWater, String paymentID) {
        //Validation of inputs
        if(ValidateHelper.validateWaterID(waterMeterID) || ValidateHelper.validateDate(date) || ValidateHelper.validatePaymentID(paymentID)){
            return;
        }
        if (amountWater < 0) {
            UserInterface.alertWindow("Error", "Water amount can not be negative");
            return;
        }
        if(!validateConsumptionReadings(waterMeterID,amountWater)){
            return;
        }
        //Make sure that the payment does not have a reading before
        if(ValidateHelper.paymentHasReadingCard(paymentID)){
            UserInterface.alertWindow("Error","Payment has a reading card attached already");
            return;
        }
        //get the waterReadingID
        DB.selectSQL("SELECT fldID FROM tblReading WHERE fldWaterMeter = '" + waterMeterID + "' AND fldDate = '" + date + "'");
        String readingID = "";
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else {
                readingID = data;
            }
        } while (true);
        //get the values for update
        String drainageTax = DatabaseHelper.findDrainageTaxByWaterReadingID(readingID);
        String waterTax = DatabaseHelper.findWaterTaxByWaterReadingID(readingID);
        //Update
        DatabaseHelper.updateRow("tblPayment", "fldID", paymentID, "fldDrainageTax", drainageTax);
        DatabaseHelper.updateRow("tblPayment", "fldID", paymentID, "fldWaterTax", waterTax);
        DatabaseHelper.updateRow("tblPayment", "fldID", paymentID, "fldReading", readingID);
    } //TODO Chek flow

    private static double findTotalConsumption(String waterMeterID) {
        //Find the last reading
        DB.selectSQL("SELECT SUM(fldAmountWater) FROM tblReading WHERE fldWaterMeter = '"+waterMeterID+"'");
        String sum = "";
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else {
                sum = data;
            }
        } while (true);
        //TODO not tested
        return Double.parseDouble(sum);
    }

    private static boolean validateConsumptionReadings(String waterMeterID, double amount) {
        boolean returnBoolean = true;
        if (findTotalConsumption(waterMeterID) <= amount){
            UserInterface.alertWindow("Error","Amount water is not possible!");
            returnBoolean = false;
        }
        //TODO not tested
        return returnBoolean;
    }
}
