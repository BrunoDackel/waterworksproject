public class Statistics {

    private static double findTotalWaterTaxes(){
        double sum = 0;
        DB.selectSQL("SELECT fldID FROM tblPayment");
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            }
            else {
                double taxRate = DatabaseHelper.findWaterTaxRateByPaymentID(data);
                double amountWater = DatabaseHelper.findAmountWaterUsedByPaymentID(data);
                sum += taxRate * amountWater;
            }
        } while (true);
        return sum;
        //TODO Not tested
    }

    private static double findTotalDrainageTaxes(){
        double sum = 0;
        DB.selectSQL("SELECT fldID FROM tblPayment");
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            }
            else {
                double taxRate = DatabaseHelper.findDrainageTaxRateByPaymentID(data);
                double amountWater = DatabaseHelper.findAmountWaterUsedByPaymentID(data);
                sum += taxRate * amountWater;
            }
        } while (true);
        return sum;
        //TODO Not tested
    }

    private static double findTotalPaid(String type,String date){
        double sum = 0;
        DB.selectSQL("SELECT SUM(fldAmount) FROM tblTransaction WHERE fldType ='"+type+"' AND fldDate = '"+date+"'");
        do {
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            }
            else {
                sum = Double.parseDouble(date);
            }
        } while (true);
        return sum;
    }

    //We DONT need this one, but it would be cool. Could use it to find all unpaid payments or whatever we want
    private static void searchTable(){
        //TODO Not implemented(Very low priority)
    }
}