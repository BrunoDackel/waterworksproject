import java.util.ArrayList;

public class Users {

    private static void removerOwnerFromBuilding(String CprNR, String buildingID){
        //Validation
        if(ValidateHelper.validateCPR(CprNR) || ValidateHelper.validateBuildingID(buildingID)){
            return;
        }
        //Update Database
        DatabaseHelper.updateRow("tblBuilding","fldID",CprNR,"fldCustomer",null);
    }

    private static void addOwnerToBuilding(String CprNR, String buildingID){
        //Validate
        if(ValidateHelper.validateCPR(CprNR) || ValidateHelper.validateBuildingID(buildingID)){
            return;
        }
        //Update Databse (With Database helper)
        DatabaseHelper.updateRow("tblBuilding","fldID",CprNR,"fldCustomer",CprNR);
        //TODO not tested
    }
}
