public class ValidateHelper {

    public static boolean validateWaterMeterID(String waterMeterID){
        boolean returnBoolean = true;
        if(!containsOnlyNumbers(waterMeterID)){
            UserInterface.alertWindow("Error","Water Meter ID "+waterMeterID+" may only contain numbers!");
            returnBoolean = false;
        }
        else if((!DatabaseHelper.searchTableForExistance("tblWaterMeter","fldID",waterMeterID))){
            UserInterface.alertWindow("Error","Water Meter ID "+waterMeterID+" is not present in DB!");
            returnBoolean = false;
        }
        else if(waterMeterID.equals("")){
            UserInterface.alertWindow("Error","Water Meter ID is empty String");
            returnBoolean = false;
        }
            return returnBoolean;
    }

    public static boolean validatePaymentID(String paymentID){
        boolean returnBoolean = true;
        if(!containsOnlyNumbers(paymentID)){
            UserInterface.alertWindow("Error","Payment ID "+paymentID+" may only contain numbers!");
            returnBoolean = false;
        }
        else if((!DatabaseHelper.searchTableForExistance("tblPayment","fldID",paymentID))){
            UserInterface.alertWindow("Error","Payment ID "+paymentID+" is not present in DB!");
            returnBoolean = false;
        }
        else if(paymentID.equals("")){
            UserInterface.alertWindow("Error","Payment ID is empty String");
            returnBoolean = false;
        }
        return returnBoolean;
    }

    public static boolean validateWaterID(String waterID){
        boolean returnBoolean = true;
        if(!containsOnlyNumbers(waterID)){
            UserInterface.alertWindow("Error","Water  ID "+waterID+" may only contain numbers!");
            returnBoolean = false;
        }
        else if((!DatabaseHelper.searchTableForExistance("tblWater","fldID",waterID))){
            UserInterface.alertWindow("Error","Water  ID "+waterID+" is not present in DB!");
            returnBoolean = false;
        }
        else if(waterID.equals("")){
            UserInterface.alertWindow("Error","Water  ID is empty String");
            returnBoolean = false;
        }
        return returnBoolean;
    }

    public static boolean validateBuildingID(String buildingID){
        boolean returnBoolean = true;
        if(!containsOnlyNumbers(buildingID)){
            UserInterface.alertWindow("Error","Building  ID "+buildingID+" may only contain numbers!");
            returnBoolean = false;
        }
        else if((!DatabaseHelper.searchTableForExistance("tblBuilding","fldID",buildingID))){
            UserInterface.alertWindow("Error","Building  ID "+buildingID+" is not present in DB!");
            returnBoolean = false;
        }
        else if(buildingID.equals("")){
            UserInterface.alertWindow("Error","Building  ID is empty String");
            returnBoolean = false;
        }
        return returnBoolean;
    }

    public static boolean validateDrainageTaxID(String drainageTaxID) {
        boolean returnBoolean = true;
        if (!containsOnlyNumbers(drainageTaxID)) {
            UserInterface.alertWindow("Error", "Drainage tax ID " + drainageTaxID + " may only contain numbers!");
            returnBoolean = false;
        } else if ((!DatabaseHelper.searchTableForExistance("tblDrainageTax", "fldID", drainageTaxID))) {
            UserInterface.alertWindow("Error", "Drainage tax ID " + drainageTaxID + " is not present in DB!");
            returnBoolean = false;
        } else if (drainageTaxID.equals("")) {
            UserInterface.alertWindow("Error", "Drainage tax ID is empty String");
            returnBoolean = false;
        }
        return returnBoolean;
    }

    public static boolean validateWaterTaxID(String waterTaxID) {
        boolean returnBoolean = true;
        if (!containsOnlyNumbers(waterTaxID)) {
            UserInterface.alertWindow("Error", "Drainage tax ID " + waterTaxID + " may only contain numbers!");
            returnBoolean = false;
        } else if ((!DatabaseHelper.searchTableForExistance("tblWaterTax", "fldID", waterTaxID))) {
            UserInterface.alertWindow("Error", "Drainage tax ID " + waterTaxID + " is not present in DB!");
            returnBoolean = false;
        } else if (waterTaxID.equals("")) {
            UserInterface.alertWindow("Error", "Drainage tax ID is empty String");
            returnBoolean = false;
        }
        return returnBoolean;
    }

    public static boolean validateCPR(String cprNR){
        boolean returnBoolean = true;
        if(cprNR.length() != 10){
            UserInterface.alertWindow("Error", "Cpr-nr "+cprNR+" is not 10 chars long");
            returnBoolean = false;
        }
        else if(!DatabaseHelper.searchTableForExistance("tblCustomer","fldCPRNR",cprNR)){
            UserInterface.alertWindow("Error", "Cpr-nr " + cprNR + " is not present in DB!");
            returnBoolean = false;
        }
        else if(!containsOnlyNumbers(cprNR)){
            UserInterface.alertWindow("Error", "Cpr-nr ID " + cprNR + " may not contain letters!");
            returnBoolean = false;
        }
        return returnBoolean;
        //TODO not tested
    }

    public static boolean validateRadingCardID(String readingCardID) {
        boolean returnBoolean = true;
        if (!containsOnlyNumbers(readingCardID)) {
            UserInterface.alertWindow("Error", "Reading card ID " + readingCardID + " may only contain numbers!");
            returnBoolean = false;
        } else if ((!DatabaseHelper.searchTableForExistance("tblReading", "fldID", readingCardID))) {
            UserInterface.alertWindow("Error", "Reading card ID " + readingCardID + " is not present in DB!");
            returnBoolean = false;
        } else if (readingCardID.equals("")) {
            UserInterface.alertWindow("Error", "Reading card ID is empty String");
            returnBoolean = false;
        }
        return returnBoolean;
    }

    public static boolean validateZip(String zip) {
        boolean returnBoolean = true;
        if (zip.length() != 4) {
            UserInterface.alertWindow("Error", "Cpr-nr " + zip + " is not 4 chars long");
            returnBoolean = false;
        } else if (!DatabaseHelper.searchTableForExistance("tblZip", "fldZip", zip)) {
            UserInterface.alertWindow("Error", "Cpr-nr " + zip + " is not present in DB!");
            returnBoolean = false;
        } else if (!containsOnlyNumbers(zip)) {
            UserInterface.alertWindow("Error", "Cpr-nr ID " + zip + " may not contain letters!");
            returnBoolean = false;
        }
        return returnBoolean;
    }

    public static boolean validateTransactionID(String transactionID) {
        boolean returnBoolean = true;
        if (!containsOnlyNumbers(transactionID)) {
            UserInterface.alertWindow("Error", "Transaction ID " + transactionID + " may only contain numbers!");
            returnBoolean = false;
        } else if ((!DatabaseHelper.searchTableForExistance("tblTransaction", "fldID", transactionID))) {
            UserInterface.alertWindow("Error", "Transaction ID " + transactionID + " is not present in DB!");
            returnBoolean = false;
        } else if (transactionID.equals("")) {
            UserInterface.alertWindow("Error", "Transaction ID is empty String!");
            returnBoolean = false;
        }
        return returnBoolean;
    }


    public static boolean validateDate(String date){
        boolean returnBoolean = true;
        //chek length
        String years = date.substring(0,4);
        String months =date.substring(5,7);
        String days = date.substring(8);
        if(date.length() != 10){
            UserInterface.alertWindow("Error", "Date has invalid length");
            returnBoolean = false;
        }
        else if((int)date.charAt(4) != 45  || date.charAt(7)!= 45){
            UserInterface.alertWindow("Error", "Date must be of format: yyyy-mm-dd");
            returnBoolean = false;
        }
        else if(!containsOnlyNumbers(years) || !containsOnlyNumbers(months)|| !containsOnlyNumbers(days)){
            UserInterface.alertWindow("Error", "may not contain numbers");
            returnBoolean = false;
        }
        else if(Integer.parseInt(months)>12){
            UserInterface.alertWindow("Error", "months may not be greater than 12");
            returnBoolean = false;
        }
        else if(Integer.parseInt(days)>31){
            UserInterface.alertWindow("Error", "days may not be greater than 31");
            returnBoolean = false;
        }
        //TODO not tested
        return true;
    }

    /**
     * this method will chek 3 things.
     * Will return false if the length is not 4
     * Will return false if the zip is already in tblCustomer
     * Will return false if the zip contains any non number chars
     * @param  zip string we want to chek
     * @return true if the String is okay
     */
    //By Sven Büchner
    public static boolean validateNewZip(String zip){
        boolean returnBoolean = true;
        if(zip.length() != 4){ // Executed when the length is not 4
            returnBoolean = false;
        }
        else if(DatabaseHelper.searchTableForExistance("tblZip","fldZip",zip)){ //executed if the zip is in the databse already
            returnBoolean = false;
        }
        else if(!containsOnlyNumbers(zip)){ //executed if the zip is contains something else then numbers
            returnBoolean = false;
        }
        return returnBoolean;
        //TODO not tested
    }

    /**
     * this method will chek 3 things.
     * Will return false if the length is not 10
     * Will return false if the cpr is already in tblCustomer
     * Will return false if the cpr contains any non number chars
     * @param cprNR the string we want to chek
     * @return true if the String is okay
     */
    //By Sven Büchner
    public static boolean validateNewCPR(String cprNR){ //TODO need error messages
        boolean returnBoolean = true;
        if(cprNR.length() != 10){ // Executed when ti length is not 10
            UserInterface.alertWindow("Error","New Cpr-nr "+cprNR+" must be 10 chars long!");
            returnBoolean = false;
        }
        else if(DatabaseHelper.searchTableForExistance("tblCustomer","fldCPRNR",cprNR)){ //executed if the cprnr is in the databse already
            UserInterface.alertWindow("Error","Cpr-nr "+cprNR+" is already in Database!");
            returnBoolean = false;
        }
        else if(!containsOnlyNumbers(cprNR)){ //executed if the cpr is contains something else then numbers
            UserInterface.alertWindow("Error","Cpr-nr "+cprNR+" may only contain numbers!");
            returnBoolean = false;
        }
        return returnBoolean;
        //TODO not tested
    }

    public static boolean validateNewWaterMeterID(String waterMeterID){
        boolean returnBoolean = true;
        if(!containsOnlyNumbers(waterMeterID)){
            UserInterface.alertWindow("Error","Water Meter ID "+waterMeterID+" may only contain numbers!");
            returnBoolean = false;
        }
        else if((DatabaseHelper.searchTableForExistance("tblWaterMeter","fldID",waterMeterID))){
            UserInterface.alertWindow("Error","Water Meter ID "+waterMeterID+" is present in DB!");
            returnBoolean = false;
        }
        else if(waterMeterID.equals("")){
            UserInterface.alertWindow("Error","Water Meter ID is empty String");
            returnBoolean = false;
        }
        return returnBoolean;
    }

    /**
     * A helper method that chek a string if it contains non numbers
     * @param str the string we want to look through
     * @return true if the string contains only numbers
     */
    //By Sven Büchner
    private static boolean containsOnlyNumbers(String str){
        boolean returnBoolean = true;
        for (int i = 0; i < str.length(); i++) {
            if ((int)str.charAt(i) < 48 || (int)str.charAt(i) > 57){ // If the ASCI is outside 48 to 57
                returnBoolean = false;
            }
        }
        return returnBoolean;
        //Todo need testing
    }

    /**
     * Test to chek if a payment has a reading card atached or not
     *
     * @param paymentID the ID of the payment, we assume this will be valid
     * @return true if the payment has a reading card attached
     */
    //By Sven Büchner
    public static boolean paymentHasReadingCard(String paymentID) {
        boolean returnBoolean = false;
        String[] tempArray = DatabaseHelper.selectRow("tblPayment", "fldID", paymentID);
        //here we test
        if (tempArray[4] != null) {
            returnBoolean = true;
        }
        return returnBoolean;
    }
}
